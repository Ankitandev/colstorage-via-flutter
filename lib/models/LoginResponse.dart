/*
{
    "statusCode": 200,
    "statusMessage": "Authenticated successfully."
}
 */

class LoginResponse {

  final int statusCode;
  final String statusMessage;

  LoginResponse(this.statusCode, this.statusMessage);

  LoginResponse.fromJson(Map<String, dynamic> json): statusCode = json['statusCode'], statusMessage = json['statusMessage'];

  Map<String, dynamic> toJson() =>
      {
        'statusCode': statusCode,
        'statusMessage':statusMessage
      };

}