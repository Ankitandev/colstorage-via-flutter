/*{
"category": "PPP",
"status": "A",
"currentDevice": "eZu71tY0d88:APA91bEbi0scS6HmQ0g2TYDcP-JaVRZkVNyHeLg6Yy5ZbwBobrDsJfmgsAnYOS3y0QdfRExpuAMr34ZhqIfQ0rE4SljPRwjWRDLC-yrf_sqZSwN9dYO0h7Tgv4kO6MXQOY7EEggdjcoC",
"imageURL": "photo-1525248289252.jpg"
}*/

class StatusResponse{
  final String category;
  final String status;
  final String currentDevice;
  final String imageURL;

  StatusResponse(this.category, this.status, this.currentDevice, this.imageURL);

  StatusResponse.fromJson(Map<String, dynamic> json)
      : category = json['category'],
        status = json['status'],
        currentDevice = json['currentDevice'],
        imageURL = json['imageURL'];

  Map<String, dynamic> toJson() =>
      {
        'category': category,
        'status': status,
        'currentDevice': currentDevice,
        'imageURL': imageURL,
      };


}