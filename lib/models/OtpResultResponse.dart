/*
"result":{
    "name":"Test PPP ",
    "staffId":"S5454523D",
    "status":"A",
    "NRIC":"",
    "cardNumber":298000009437,
    "lastLogin":"2018-06-19T04:00:00.000Z",
    "authMethod":"S",
    "imageURL":"photo-1525248289252.jpg",
    "category":"PPP"
    }
 */

class OtpResultResponse {
  final String name;
  final String staffId;
  final String status;
  final String NRIC;
  final int cardNumber;
  final String lastLogin;
  final String authMethod;
  final String imageURL;
  final String category;

  OtpResultResponse(
      this.name,
      this.staffId,
      this.status,
      this.NRIC,
      this.cardNumber,
      this.lastLogin,
      this.authMethod,
      this.imageURL,
      this.category);

  OtpResultResponse.fromJson(Map<dynamic, dynamic> json)
      : name = json['name'],
        staffId = json['staffId'],
        status = json['status'],
        NRIC = json['NRIC'],
        cardNumber = json['cardNumber'],
        lastLogin = json['lastLogin'],
        authMethod = json['authMethod'],
        imageURL = json['imageURL'],
        category = json['category'];

  Map<dynamic, dynamic> toJson() => {
        'name': name,
        'staffId': staffId,
        'status': status,
        'NRIC': NRIC,
        'cardNumber': cardNumber,
        'lastLogin': lastLogin,
        'authMethod': authMethod,
        'imageURL': imageURL,
        'category': category,
      };
}
