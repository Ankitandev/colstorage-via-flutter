class User {
  final String staffId;
  final String name;
  final String nric;
  final String staffType;
  final String phone;


  User(this.staffId, this.name, this.nric, this.staffType, this.phone);

  User.fromJson(Map<String, dynamic> json)
      : staffId = json['staffId'],
        name = json['name'],
        nric = json['nric'],
        staffType = json['staffType'],
        phone = json['phone'];

  Map<String, dynamic> toJson() =>
      {
        'staffId': staffId,
        'name': name,
        'nric': nric,
        'staffType': staffType,
        'phone': phone,
      };
}