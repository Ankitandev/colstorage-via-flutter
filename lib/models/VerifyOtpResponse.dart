import 'package:coldstorage_singapore/models/OtpResultResponse.dart';
/*
{"statusCode":200,
"statusMessage":"Authenticated successfully.",
"result":{
    "name":"Test PPP ",
    "staffId":"S5454523D",
    "status":"A","NRIC":"",
    "cardNumber":298000009437,
    "lastLogin":"2018-06-19T04:00:00.000Z",
    "authMethod":"S",
    "imageURL":"photo-1525248289252.jpg",
    "category":"PPP"
    }
 }

{"statusCode":404,"statusMessage":"OTP already used. Please retry."}

{"statusCode":404,"statusMessage":"Incorrect OTP."}
 */

class VerifyOtpResponse{
  final int statusCode;
  final String statusMessage;
  final String result;

  VerifyOtpResponse(this.statusCode, this.statusMessage, this.result);

  VerifyOtpResponse.fromJson(Map<String, dynamic> json)
      : statusCode = json['statusCode'],
        statusMessage = json['statusMessage'],
        result = json['result'];


  Map<String, dynamic> toJson() =>
      {
        'statusCode': statusCode,
        'statusMessage': statusMessage,
        'result': result
      };

}


