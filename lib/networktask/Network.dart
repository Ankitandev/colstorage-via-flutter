import 'dart:async';

import 'package:http/http.dart' as http;
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:coldstorage_singapore/models/UserStatus.dart';
import 'package:coldstorage_singapore/models/VerifyOtpResponse.dart';
import 'dart:convert';

class Network {
  Future<String> getJsonData() async {
    var url = CustomStrings.URL_JSON_DATA;
    var res;
    http.get(url).then((response) {
      res = response;
      print("Response status: ${response.statusCode}");
      print("Response body: ${response.body}");
    });
    return res;
  }

  Future<String> postLoginData(String staffId, String name, String phone,
      String NRIC, String userType) async {
    var res;
    var url = CustomStrings.BASE_URL + "/user/login";
    print(url);
    http.post(Uri.encodeFull(url), body: {
      "staffId": staffId,
      "name": name,
      "phone": phone,
      "userType": userType
    }).then((response) {
      print(response.body);
      res = response.body;
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print("completed");
    });
    return res;
  }

//  Future<String> verifyOtp(String staffId, String OTP, String deviceType,
//      String deviceToken, String userType) async {
//    var res;
//    var url = CustomStrings.BASE_URL + "/user/verifyOTP";
//    print(url);
//    http.post(Uri.encodeFull(url), body: {
//      "staffId": staffId,
//      "OTP": OTP,
//      "deviceType": deviceType,
//      "deviceToken": deviceToken,
//      "userType": userType
//    }).then((response) {
//      print(response.body);
//      Map verifyOtpMap = json.decode(response.body);
//      var verifyOtpResponse = new VerifyOtpResponse.fromJson(verifyOtpMap);
//      print(verifyOtpResponse.statusCode);
//      print(verifyOtpResponse.statusMessage);
//      print(verifyOtpResponse.result.imageURL);
//      print(verifyOtpResponse.result.status);
//      print(verifyOtpResponse.result.cardNumber);
//      res = response.body;
//    }).catchError((error) {
//      print(error);
//    }).whenComplete(() {
//      print("completed");
//    });
//    return res;
//  }

  Future<String> resendOtp(String staffId, String phone) async {
    var res;
    var url = CustomStrings.BASE_URL + "/user/resendOTP";
    print(url);
    http.post(Uri.encodeFull(url),
        body: {"staffId": staffId, "phone": phone}).then((response) {
      print(response.body);
      res = response.body;
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print("completed");
    });
    return res;
  }

  Future<String> fraudUser(
      String Staff_Name, String Staff_Id, String Device) async {
    var res;
    var url = CustomStrings.BASE_URL + "/user/fraudUser";
    print(url);
//    String body = "{'Staff_Name':'abc', 'Staff_Id':'1234567', 'Device':'android'}";
    http.post(Uri.encodeFull(url),
//        headers: {"Content-Type":"application/json"},
        body: {
          "Staff_Name": Staff_Name,
          "Staff_Id": Staff_Id,
          "Device": Device
        }).then((response) {
      print(response.body);
      res = response.body;
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print("completed");
    });
    return res;
  }

  Future<String> updateImage(String path) async {
    var res;
    var url = Uri.parse(CustomStrings.BASE_URL + "/user/uploadPhoto");
    print(url);
    var request = new http.MultipartRequest("POST", url);
    request.files.add(new http.MultipartFile.fromString('memberCardNo', path,
        contentType: new MediaType('application', 'x-tar')));
    request.send().then((response) {
      if (response.statusCode == 200) print("Uploaded!");
    });
    return res;
  }

  Future<Map> getStatus(String staffId, String userType) async {
    var url = CustomStrings.BASE_URL + "/user/status/" + staffId + "/" + userType;
    print(url);
    try {
      Response r = await http.get(
        url,
        headers: {
          'Accept': 'application/json',
        },
      );
      print(r.body);
      Map userMap = json.decode(r.body);
      var userStatus = new StatusResponse.fromJson(userMap);
      print(userStatus.category);
      print(userStatus.currentDevice);
      print(userStatus.imageURL);
      print(userStatus.status);
      return json.decode(r.body);
    } catch (e) {
      return {'error': e.toString()};
    }

  }

}