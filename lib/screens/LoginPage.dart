import 'dart:convert';

import 'package:coldstorage_singapore/screens/ProfilePage.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:flutter/material.dart';
import 'package:coldstorage_singapore/values/CustomColors.dart';
import 'package:coldstorage_singapore/screens/OtpVerificationPage.dart';
import 'package:coldstorage_singapore/values/CustomIntegers.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:coldstorage_singapore/networktask/Network.dart';
import 'package:http/http.dart' as http;
import 'package:coldstorage_singapore/models/LoginResponse.dart';

void main() {

  runApp(new MaterialApp(
    home: new LoginPage(),
  ));

}


/// home page with stateful widget
class LoginPage extends StatefulWidget {
  const LoginPage({
    this.onSaved,
  });

  final FormFieldSetter<String> onSaved;

  @override
  LoginState createState() => new LoginState();
}


/// class my home page to hit api
class LoginState extends State<LoginPage> {
  MethodChannel methodChannel = const MethodChannel(CustomStrings.CS_CHANNEL);
  var _isChecked = false;
  var nameController = new TextEditingController();
  var idController = new TextEditingController();
  var nricController = new TextEditingController();
  var typeController = new TextEditingController();
  var numController = new TextEditingController();
  UserData userData = new UserData();
  Network network = new Network();

  @override
  void initState() {
    super.initState();
//    _checkLoggedInState();
  }



  @override
  void dispose() {
    super.dispose();
  }

  Future<Null> _createToast(String toastText) async {
    print(toastText);
    try {
      await methodChannel.invokeMethod('createToast', {"text": toastText});
    } on PlatformException {
      print('failed to connect...');
    }
    on MissingPluginException {
      print('missing plugin exception');
    }
  }

  _navigateToOtpPage() {
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration:
            const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new OtpVerification();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }));
  }

  _startLoginProcess() {
    userData.staffId = idController.text;
    userData.name = nameController.text;
    userData.staffType = typeController.text;
    userData.nric = nricController.text;
    userData.phone = numController.text;

//    print('controller data is ${idController.text}');
    print("staff id is ${userData.staffId}");
    print("name is ${userData.name}");
    print("staffType is ${userData.staffType}");
    print("nric is ${userData.nric}");
    print("phone number is ${userData.phone}");
    if (_isValid()) {
      _saveDataInPreferences();
      _postLoginData(userData.staffId, userData.name, "+65${userData.phone}",
          userData.nric, "ST");
    }
  }

  Future<String> _postLoginData(String staffId, String name, String phone,
      String NRIC, String userType) async {
    var res;
    var url = CustomStrings.BASE_URL + "/user/login";
    print(url);
    http.post(Uri.encodeFull(url), body: {
      "staffId": staffId,
      "name": name,
      "phone": phone,
      "userType": userType
    }).then((response) {
      print(response.body);
      res = response.body;
      Map loginMap = json.decode(response.body);
      var loginResponse = new LoginResponse.fromJson(loginMap);
      if (loginResponse.statusCode == 200) {
        _createToast(loginResponse.statusMessage);
        _navigateToOtpPage();
      }
      else{
        _createToast(loginResponse.statusMessage);
      }
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print("login response received.");
    });
    return res;
  }

  _saveDataInPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('staffId', userData.staffId);
    preferences.setString('name', userData.name);
    preferences.setString('staffType', userData.staffType);
    preferences.setString('nric', userData.nric);
    preferences.setString('phone', userData.phone);

    print("its getting saved..." + preferences.getString('staffId'));
  }

  bool _isValid() {
    if (userData.staffId.isEmpty) {
      return false;
    } else if (userData.staffType.isEmpty) {
      return false;
    } else if (userData.nric.isEmpty && userData.name.isEmpty) {
      return false;
    } else if (userData.phone.isEmpty) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double screenWidth = MediaQuery.of(context).size.width;

    Border border = new Border.all(
        color: CustomColors.borderGray, width: 1.0, style: BorderStyle.solid);

    BoxDecoration decoration = new BoxDecoration(
      shape: BoxShape.rectangle,
      color: CustomColors.gray,
      borderRadius: new BorderRadius.all(const Radius.circular(2.0)),
      border: border,
    );

    return new Scaffold(
        appBar: null,
        body: new SingleChildScrollView(
          child: new Container(
              margin: MediaQuery.of(context).padding,
              width: screenWidth,
              child: new Container(
                decoration: new BoxDecoration(
                  color: Colors.white,
                  image: new DecorationImage(
                    image: new AssetImage("resources/images/bg_login.png"),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                padding: EdgeInsets.fromLTRB(16.0, 50.0, 16.0, 16.0),
                child: new Column(
                  children: <Widget>[
                    new Image.asset(
                      'resources/images/logo_login.png',
                      width: 150.0,
                      height: 90.0,
                    ),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 60.0, 2.0, 16.0),
                        width: screenWidth,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        decoration: decoration,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 30.0,
                                height: 30.0,
                                padding: EdgeInsets.all(7.0),
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(16.0),
                                  color: CustomColors.roundBlue,
                                ),
                                child: new Image.asset(
                                  "resources/images/ic_staffid.png",
                                ),
                              ),
                              new Flexible(
                                child: new TextField(
                                  controller: idController,
                                  textAlign: TextAlign.start,
                                  keyboardType: TextInputType.text,
                                  maxLines: 1,
                                  onSubmitted: (String value) {
                                    userData.staffId = value;
                                  },
                                  style: new TextStyle(
                                      fontSize: 18.0, color: Colors.black),
                                  decoration: new InputDecoration(
                                      hintText: 'Enter the staff Id',
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          14.0, 0.0, 0.0, 2.0)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 16.0),
                        width: 500.0,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        decoration: decoration,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 30.0,
                                height: 30.0,
                                padding: EdgeInsets.all(7.0),
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(16.0),
                                  color: CustomColors.roundBlue,
                                ),
                                child: new Image.asset(
                                  "resources/images/ic_name.png",
                                ),
                              ),
                              new Flexible(
                                child: new TextField(
                                  controller: nameController,
                                  textAlign: TextAlign.start,
                                  keyboardType: TextInputType.text,
                                  maxLines: 1,
                                  onSubmitted: (String value) {
                                    userData.name = value;
                                  },
                                  style: new TextStyle(
                                      fontSize: 18.0, color: Colors.black),
                                  decoration: new InputDecoration(
                                      hintText: 'Enter your name',
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          14.0, 0.0, 0.0, 2.0)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 16.0),
                        width: 500.0,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        decoration: decoration,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 30.0,
                                height: 30.0,
//                                padding: EdgeInsets.all(7.0),
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(16.0),
                                  color: CustomColors.roundBlue,
                                ),
                                child: new Image.asset(
                                  "resources/images/ic_staff.png",
                                ),
                              ),
                              new Flexible(
                                child: new TextField(
                                  controller: typeController,
                                  textAlign: TextAlign.start,
                                  keyboardType: TextInputType.text,
                                  maxLines: 1,
                                  onSubmitted: (String value) {
                                    userData.staffType = value;
                                  },
                                  style: new TextStyle(
                                      fontSize: 18.0, color: Colors.black),
                                  decoration: new InputDecoration(
                                      hintText: 'Staff',
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          14.0, 0.0, 0.0, 2.0)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 16.0),
                        width: 500.0,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        decoration: decoration,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 30.0,
                                height: 30.0,
                                padding: EdgeInsets.all(7.0),
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(16.0),
                                  color: CustomColors.roundBlue,
                                ),
                                child: new Image.asset(
                                  "resources/images/ic_digit.png",
                                ),
                              ),
                              new Flexible(
                                child: new TextField(
                                  controller: nricController,
                                  textAlign: TextAlign.start,
                                  keyboardType: TextInputType.text,
                                  maxLines: 1,
                                  onSubmitted: (String value) {
                                    userData.nric = value;
                                  },
                                  style: new TextStyle(
                                      fontSize: 18.0, color: Colors.black),
                                  decoration: new InputDecoration(
                                      hintText: 'Last 4-digit of NRIC Number',
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          14.0, 0.0, 0.0, 2.0)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 16.0),
                        width: 500.0,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        decoration: decoration,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Container(
                                width: 30.0,
                                height: 30.0,
                                padding: EdgeInsets.all(7.0),
                                decoration: new BoxDecoration(
                                  borderRadius: new BorderRadius.circular(16.0),
                                  color: CustomColors.roundBlue,
                                ),
                                child: new Image.asset(
                                  "resources/images/ic_phone.png",
                                ),
                              ),
                              new Flexible(
                                child: new TextField(
                                  controller: numController,
                                  textAlign: TextAlign.start,
                                  keyboardType: TextInputType.phone,
                                  maxLines: 1,
                                  onSubmitted: (String value) {
                                    userData.phone = value;
                                  },
                                  style: new TextStyle(
                                      fontSize: 18.0, color: Colors.black),
                                  decoration: new InputDecoration(
                                      hintText: 'Please enter valid number',
                                      border: InputBorder.none,
                                      contentPadding: EdgeInsets.fromLTRB(
                                          14.0, 0.0, 0.0, 2.0)),
                                ),
                              ),
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 16.0),
                        width: 500.0,
                        padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              new Checkbox(
                                  activeColor: CustomColors.appThemeBlue,
                                  value: _isChecked,
                                  onChanged: (bool value) {
                                    setState(() {
                                      _isChecked = value;
                                    });
                                  }),
                              new Text(
                                "Yes, I agree to terms of use and privacy policy",
                                style: new TextStyle(
                                  fontSize: 12.0,
                                ),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        )),
                    new Container(
                        margin: EdgeInsets.fromLTRB(4.0, 0.0, 4.0, 12.0),
                        padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: CustomColors.buttonRed,
                          borderRadius:
                              new BorderRadius.all(const Radius.circular(2.0)),
                        ),
                        child: new Row(
                          children: <Widget>[
                            new Expanded(
                              child: new FlatButton(
                                onPressed: _startLoginProcess,
//                                onPressed: _createToast,

                                ///_navigateToOtpPage,
                                color: CustomColors.buttonRed,
                                textColor: Colors.white,
                                disabledTextColor: Colors.white,
                                disabledColor: CustomColors.buttonRed,
                                child: new Text("SUBMIT"),
                              ),
                            )
                          ],
                        ))
                  ],
                ),
              )),
        ));
  }
}

class UserData {
  String staffId = '';
  String name = '';
  String staffType = '';
  String nric = '';
  String phone = '';
}
