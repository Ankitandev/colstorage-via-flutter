import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html_view/flutter_html_view.dart';
import 'package:coldstorage_singapore/utilityclasses/HtmlPages.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:coldstorage_singapore/values/CustomColors.dart';

void main() {
  runApp(new MaterialApp(
    home: new PrivacyPolicyPage(),
  ));
}

class PrivacyPolicyPage extends StatefulWidget {
  @override
  PrivacyPolicyState createState() => PrivacyPolicyState();
}

class PrivacyPolicyState extends State<PrivacyPolicyPage> {
  var _appBarTitle;
  String _htmlFileText = HtmlPages.aboutUs;

  @override
  void initState() {
    super.initState();

    _appBarTitle = CustomStrings.privacyPolicy;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(_appBarTitle, style: TextStyle(fontSize: 15.0),),
        backgroundColor: CustomColors.appThemeBlue,
      ),
      body: new Stack(
        children: <Widget>[
          new Positioned(
            top: 0.0,
            bottom: 100.0,
            child: new Container(
              width: screenWidth,
              height: screenHeight,
              child: new SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: new HtmlView(data: _htmlFileText),
              ),
            ),
          ),
          new Positioned(
            bottom: 0.0,
            child: new Container(
              width: screenWidth,
              height: 100.0,
              color: Colors.black12,
              padding: EdgeInsets.all(20.0),
              child: new FlatButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                color: CustomColors.buttonRed,
                textColor: Colors.white,
                disabledTextColor: Colors.white,
                disabledColor: CustomColors.buttonRed,
                child: new Text(CustomStrings.tncClose),
              ),
            ),
          )
        ],
      ),
    );
  }
}
