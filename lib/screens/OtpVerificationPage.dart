import 'dart:async';
import 'dart:convert';
import 'package:coldstorage_singapore/models/OtpResultResponse.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:coldstorage_singapore/models/VerifyOtpResponse.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:flutter/material.dart';
import 'package:coldstorage_singapore/values/CustomColors.dart';
import 'package:coldstorage_singapore/screens/ProfilePage.dart';
import 'package:coldstorage_singapore/values/CustomIntegers.dart';
import 'package:sms/sms.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(new MaterialApp(
    home: new OtpVerification(),
    routes: <String, WidgetBuilder> {
      '/': (BuildContext context) => new ProfilePage(),//Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    },
  ));
}

class OtpVerification extends StatefulWidget {
  @override
  OtpVerificationState createState() => new OtpVerificationState();
}

class OtpVerificationState extends State<OtpVerification>
    with TickerProviderStateMixin {
//  final myController = new TextEditingController();
  final FocusNode focusNode2 = new FocusNode();
  final FocusNode focusNode3 = new FocusNode();
  final FocusNode focusNode4 = new FocusNode();
  var firstDigitController = new TextEditingController();
  var secondDigitController = new TextEditingController();
  var thirdDigitController = new TextEditingController();
  var fourthDigitController = new TextEditingController();
  var staffId, name, nric, staffType, phone;
  MethodChannel methodChannel = const MethodChannel(CustomStrings.CS_CHANNEL);

  AnimationController _controller;
  static const int kStartValue = 45;
  SmsReceiver receiver = new SmsReceiver();

//  _printLatestValue() {
//    print("Second text field: ${myController.text}");
//  }

  @override
  void initState() {
    super.initState();
    // Start listening to changes
//    myController.addListener(_printLatestValue);
    _controller = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: kStartValue),
    );
    _controller.forward(from: 0.0);
    receiver.onSmsReceived.listen((SmsMessage msg) => _getOtpFromSMS(msg.body));
    _getDataFromPreferences();
  }


  Future<Null> _createToast(String toastText) async {
    print(toastText);
    try {
      await methodChannel.invokeMethod('createToast', {"text": toastText});
    } on PlatformException {
      print('failed to connect...');
    }
    on MissingPluginException {
      print('missing plugin exception');
    }
  }

  /// Method to get data from preferences
  _getDataFromPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    staffId = preferences.getString('staffId');
    name = preferences.getString('name');
    nric = preferences.getString('staffType');
    staffType = preferences.getString('nric');
    phone = preferences.getString('phone');
  }


  /// Method to get OTP from SMS in android
  _getOtpFromSMS(String messageBody) {
    int index;
    print(messageBody);
    messageBody = messageBody.substring(messageBody.indexOf("OTP"));
    for (int i = 0; i < messageBody.length; i++) {
      if (messageBody.codeUnitAt(i) > 47 && messageBody.codeUnitAt(i) < 59) {
        index = i;
        break;
      }
    }
    print(index);
    var otp = messageBody.substring(index, index + 4);
    print(otp);
    if (otp.isNotEmpty) {
      firstDigitController.text = otp.substring(0, 1);
      secondDigitController.text = otp.substring(1, 2);
      thirdDigitController.text = otp.substring(2, 3);
      fourthDigitController.text = otp.substring(3, 4);
    }
  }


  _startOtpVerificationProcess(){

    if(_isValid()) {
      var otp = firstDigitController.text + secondDigitController.text + thirdDigitController.text + fourthDigitController.text;
      _verifyOtp(staffId, otp, "android", "randomdevicetoken", staffType);
    }
    else{
      _createToast('Otp field is empty!');
    }
  }

  _isValid(){
    if (firstDigitController.text.isEmpty) {
      return false;
    } else if (secondDigitController.text.isEmpty) {
      return false;
    } else if (thirdDigitController.text.isEmpty) {
      return false;
    } else if (fourthDigitController.text.isEmpty) {
      return false;
    }
    return true;
  }

  var _category, _imageURL, _cardNumber, _status;
//  UserData userData = new UserData();

  _saveDataInPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setInt('cardNumber', _cardNumber);
    preferences.setString('imageURL', _imageURL);
    preferences.setString('category', _category);
    preferences.setString('status', _status);
    _navigateToNextPage();
  }


  _verifyOtp(String staffId, String otp, String deviceType,
      String deviceToken, String userType) async {
//    var res;
    var url = CustomStrings.BASE_URL + "/user/verifyOTP";
    print(url);
    http.post(Uri.encodeFull(url), body: {
      "staffId": staffId,
      "OTP": otp,
      "deviceType": deviceType,
      "deviceToken": deviceToken,
      "userType": userType
    }).then((response) {
      print(response.body);
      Map verifyOtpMap = json.decode(response.body);

      print(verifyOtpMap['statusCode']);
      if(verifyOtpMap['statusCode'] == 200){
        print(verifyOtpMap['result']['imageURL']);
        print(verifyOtpMap['result']['cardNumber']);
        _cardNumber = verifyOtpMap['result']['cardNumber'];
        _imageURL = verifyOtpMap['result']['imageURL'];
        _category = verifyOtpMap['result']['category'];
        _status = verifyOtpMap['result']['status'];


        _createToast(verifyOtpMap['statusMessage']);
        _saveDataInPreferences();
      }
      else{

        _createToast(verifyOtpMap['statusMessage']);
      }


//      print(verifyOtpMap);
//      var verifyOtpResponse = new VerifyOtpResponse.fromJson(verifyOtpMap);
//      print(verifyOtpResponse);
//      print(verifyOtpResponse.statusCode);
////      verifyOtpResponse.result != null ? print(verifyOtpResponse.result.staffId): print('result is null');
//
//      Map resultMap = json.decode(verifyOtpResponse.result);
//      var resultResponse = new OtpResultResponse.fromJson(resultMap);
//      if(verifyOtpResponse.statusCode == 200) {
//
//        _cardNumber = resultResponse.cardNumber;
//        _imageURL = resultResponse.imageURL;
//        _category = resultResponse.category;
//        _status = resultResponse.status;
//        _createToast(resultResponse.status);
//        _saveDataInPreferences();
//      }
//      else{
//        _createToast(verifyOtpResponse.statusMessage);
//      }
//      res = response.body;
    }).catchError((error) {
      print(error);
    }).whenComplete(() {
      print("completed");
    });
  }


  /// Method to navigate to next page if otp is correct
  _navigateToNextPage() {
//    Navigator.of(context).push(new PageRouteBuilder(
//        opaque: true,
//        transitionDuration:
//            const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
//        pageBuilder: (BuildContext context, _, __) {
//          return new ProfilePage();
//        },
//        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
//          return new SlideTransition(
//            child: child,
//            position: new Tween<Offset>(
//              begin: const Offset(2.0, 0.0),
//              end: Offset.zero,
//            ).animate(animation),
//          );
//        }));

  Navigator.of(context).pushAndRemoveUntil(new PageRouteBuilder(
      opaque: true,
      transitionDuration:
      const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
      pageBuilder: (BuildContext context, _, __) {
        return new ProfilePage();
      },
      transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
        return new SlideTransition(
          child: child,
          position: new Tween<Offset>(
            begin: const Offset(2.0, 0.0),
            end: Offset.zero,
          ).animate(animation),
        );
      }), (Route<dynamic> route) => false);

//  Navigator.of(context).pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
//    myController.dispose();
    firstDigitController.dispose();
    secondDigitController.dispose();
    thirdDigitController.dispose();
    fourthDigitController.dispose();
    _controller.dispose();
    focusNode2.dispose();
    focusNode3.dispose();
    focusNode4.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return new Scaffold(
      body: new Container(
          margin: MediaQuery.of(context).padding,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new BackButton(
                color: CustomColors.appThemeBlue,
              ),
              new Row(
                children: <Widget>[
                  Expanded(
                    child: new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            "Please Enter a 4-Digit Verification Code \nsent to your mobile number.",
                            maxLines: 2,
                            textAlign: TextAlign.center,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              new Container(
                width: screenWidth,
                alignment: Alignment.center,
                child: new Row(
                  children: <Widget>[
                    new Flexible(
                      child: new Container(
                        width: screenWidth / 4,
                        margin: EdgeInsets.fromLTRB(16.0, 30.0, 8.0, 10.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                              width: 65.0,
                              height: 70.0,
                              alignment: Alignment.center,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: CustomColors.gray,
                                borderRadius: new BorderRadius.all(
                                    const Radius.circular(2.0)),
                                border: new Border.all(
                                    color: CustomColors.borderGray,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                              child: new TextField(
                                controller: firstDigitController,
                                onChanged: (text) {
                                  print("First text field: $text");
                                  FocusScope
                                      .of(context)
                                      .requestFocus(focusNode2);
                                },
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                maxLength: 1,
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 36.0),
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Flexible(
                      child: new Container(
                        width: screenWidth / 4,
                        margin: EdgeInsets.fromLTRB(8.0, 30.0, 8.0, 10.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                              width: 65.0,
                              height: 70.0,
                              alignment: Alignment.center,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: CustomColors.gray,
                                borderRadius: new BorderRadius.all(
                                    const Radius.circular(2.0)),
                                border: new Border.all(
                                    color: CustomColors.borderGray,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                              child: new TextField(
                                controller: secondDigitController,
                                onChanged: (text) {
                                  print("First text field: $text");
                                  FocusScope
                                      .of(context)
                                      .requestFocus(focusNode3);
                                },
                                focusNode: focusNode2,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                maxLength: 1,
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 36.0),
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Flexible(
                      child: new Container(
                        width: screenWidth / 4,
                        margin: EdgeInsets.fromLTRB(8.0, 30.0, 8.0, 10.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                              width: 65.0,
                              height: 70.0,
                              alignment: Alignment.center,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: CustomColors.gray,
                                borderRadius: new BorderRadius.all(
                                    const Radius.circular(2.0)),
                                border: new Border.all(
                                    color: CustomColors.borderGray,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                              child: new TextField(
                                controller: thirdDigitController,
                                onChanged: (text) {
                                  print("First text field: $text");
                                  FocusScope
                                      .of(context)
                                      .requestFocus(focusNode4);
                                },
                                focusNode: focusNode3,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                maxLength: 1,
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 36.0),
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Flexible(
                      child: new Container(
                        width: screenWidth / 4,
                        margin: EdgeInsets.fromLTRB(8.0, 30.0, 16.0, 10.0),
                        child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                              width: 65.0,
                              height: 70.0,
                              alignment: Alignment.center,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: CustomColors.gray,
                                borderRadius: new BorderRadius.all(
                                    const Radius.circular(2.0)),
                                border: new Border.all(
                                    color: CustomColors.borderGray,
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                              child: new TextField(
                                controller: fourthDigitController,
                                onChanged: (text) {
                                  print("First text field: $text");
                                  FocusScope
                                      .of(context)
                                      .requestFocus(new FocusNode());
                                },
                                focusNode: focusNode4,
                                keyboardType: TextInputType.number,
                                textAlign: TextAlign.center,
                                maxLines: 1,
                                maxLength: 1,
                                style: new TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 36.0),
                                decoration: new InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              new Container(
                  margin: EdgeInsets.fromLTRB(20.0, 40.0, 20.0, 12.0),
                  padding: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                  decoration: new BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: CustomColors.buttonRed,
                    borderRadius:
                        new BorderRadius.all(const Radius.circular(2.0)),
                  ),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new FlatButton(
                          onPressed: _startOtpVerificationProcess,
                          color: CustomColors.buttonRed,
                          textColor: Colors.white,
                          disabledTextColor: Colors.white,
                          disabledColor: CustomColors.buttonRed,
                          child: new Text("VERIFY NOW"),
                        ),
                      )
                    ],
                  )),
              new Row(
                children: <Widget>[
                  Expanded(
                    child: new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            "Didn't receive verification code?",
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                          new InkWell(
                            child: Text(
                              "  RESEND",
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                  color: CustomColors.appThemeBlue,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              new Row(
                children: <Widget>[
                  Expanded(
                    child: new Container(
                      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new Text(
                            "Time Left : ",
                            maxLines: 1,
                            textAlign: TextAlign.center,
                          ),
                          new Container(
                            child: new Center(
                              child: new Countdown(
                                animation: new StepTween(
                                  begin: kStartValue,
                                  end: 0,
                                ).animate(_controller),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ],
          )),
    );
  }
}

class Countdown extends AnimatedWidget {
  Countdown({Key key, this.animation}) : super(key: key, listenable: animation);
  Animation<int> animation;

  @override
  build(BuildContext context) {
    if (animation.value.toString().length == 1)
      return new Text(
        "00:0" + animation.value.toString(),
        style: new TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.bold,
            color: CustomColors.buttonRed),
      );
    else
      return new Text(
        "00:" + animation.value.toString(),
        style: new TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.bold,
            color: CustomColors.buttonRed),
      );
  }


}
class UserData {
  int cardNumber = 0;
  String imageURL = '';
  String category = '';
  String status = '';
}
