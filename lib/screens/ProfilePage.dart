import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:coldstorage_singapore/values/CustomColors.dart';
import 'package:coldstorage_singapore/screens/SettingsPage.dart';
import 'dart:async';
import 'package:coldstorage_singapore/values/CustomIntegers.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(new MaterialApp(
    home: new ProfilePage(),
  ));
}

/// home page with stateful widget
class ProfilePage extends StatefulWidget {
  @override
  ProfileState createState() => new ProfileState();
}

/// class my home page to hit api
class ProfileState extends State<ProfilePage> {
  static const MethodChannel methodChannel = const MethodChannel(CustomStrings.CS_CHANNEL);
  TextEditingController hourController = new TextEditingController();
  TextEditingController minutesController = new TextEditingController();
  TextEditingController secondsController = new TextEditingController();
  File _image;
  Timer timer;
  Uint8List _byteData;
  BarcodeScanner scanner = new BarcodeScanner();
  int _cardNumber;
  String _status;
  int _staffId;
  String _category;
  String _imageURL, _name;

  @override
  void initState() {
    super.initState();
    const oneSec = const Duration(seconds: 1);
    timer = new Timer.periodic(oneSec, (Timer t) => _getCurrentTime());
    _getDataFromPreferences();
    _saveLoggedInState();
  }



  _saveLoggedInState() async{

    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('isLoggedIn', true);
  }

  Future<Null> _generateBarcode() async {
//    print(_cardNumber);
//    String text = '298000009437';
    try {
      Uint8List byteData =
          await methodChannel.invokeMethod('getBarcode', {"id": '298000009437'});
      print('byte data is below');
      print(byteData);
      setState(() {
        _byteData = byteData;
      });
    } on PlatformException {
      print('failed to connect...');
    }
  }

  Future getImage(BuildContext context, bool isGallery) async {
    var image = await ImagePicker.pickImage(
        source: isGallery ? ImageSource.gallery : ImageSource.camera);
    setState(() {
      _image = image;
    });
    Navigator.pop(context);

    _generateBarcode();
  }

  _navigateToSettingsPage() {
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration:
            const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new SettingsPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }));
  }

  _getDataFromPreferences() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var cardNumber = preferences.getInt('cardNumber');
    var imageURL = preferences.getString('imageURL');
    var category = preferences.getString('category');
    var status = preferences.getString('status');
    var name = preferences.getString('name');
    var staffId = preferences.getInt('staffId');

    print(cardNumber);
    print(imageURL);
    print(category);
    print(status);
//    print(name);
    print(staffId);

    setState(() {
      _cardNumber = cardNumber;
      _imageURL = imageURL;
      _category = category;
      _status = status;
      _name = name;
      _staffId = staffId;
    });


  }

  _getCurrentTime() {
    var now = new DateTime.now();
    var hour = now.hour.toString();
    var minutes = now.minute.toString();
    var seconds = now.second.toString();

    try {
      hourController.text = _formatCurrentTimeText(hour);
      minutesController.text = _formatCurrentTimeText(minutes);
      secondsController.text = _formatCurrentTimeText(seconds);
    } catch (e) {
      print(e);
    }
  }

  String _formatCurrentTimeText(String time) {
    time.length == 1 ? time = "0" + time : time = time;
    return time;
  }

  @override
  void dispose() {
    super.dispose();
    hourController.dispose();
    minutesController.dispose();
    secondsController.dispose();
    timer.cancel();
  }


  _testPrint(){
    print('hello');
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return new Scaffold(
      body: new SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
        scrollDirection: Axis.vertical,
        child: new Flex(
          direction: Axis.vertical,
          children: <Widget>[
            new Container(
              height: 350.0,
              width: screenWidth,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("resources/images/bg_home.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: new Column(
                children: <Widget>[
                  //new IconButton.icon('resources/images/ic_setting.png', width: 30.0, height: 30.0,alignment: Alignment.topRight, )
                  new Container(
                    padding: EdgeInsets.fromLTRB(16.0, 32.0, 14.0, 16.0),
                    width: screenWidth,
                    child: new IconButton(
                      icon: new Image.asset('resources/images/ic_setting.png'),
                      onPressed: _navigateToSettingsPage,
                      alignment: Alignment.topRight,
                    ),
                  ),
                  new InkWell(
                    onTap: () => showDialog(
                        context: context,
                        builder: (_) => new AlertDialog(
                          title: new Container(
                            alignment: Alignment.center,
                            child: new Text(
                              "Choose your action",
                              style: TextStyle(
                                  fontSize: 16.0, color: Colors.black),
                            ),
                          ),
                          content: new Container(
                              child: new Flex(
                                direction: Axis.horizontal,
                                mainAxisAlignment:
                                MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  FlatButton(
                                    onPressed: () => getImage(context, false),
                                    child: Text(
                                      'CAMERA',
                                      style: TextStyle(
                                          fontSize: 14.0,
                                          color: CustomColors.appThemeBlue),
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () => getImage(context, true),
                                    child: Text('GALLERY',
                                        style: TextStyle(
                                            fontSize: 14.0,
                                            color: CustomColors.appThemeBlue)),
                                  ),
                                ],
                              )),
                        )),
                    child: new Container(
                        margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
                        width: 120.0,
                        height: 120.0,
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                              fit: BoxFit.cover,
                              image: _imageURL != null ?
                              NetworkImage(CustomStrings.BASE_URL + '/user/getImage/' + _imageURL) :
                              _image == null ?
                              new AssetImage('resources/images/img_avatar_active.png') :
                              FileImage(_image),
                          ),
                          shape: BoxShape.circle,
                          border: new Border.all(
                              color: Colors.greenAccent,
                              width: 3.5,
                              style: BorderStyle.solid),
                        )),
                  ),
                  new Container(
                    margin: EdgeInsets.all(10.0),
                    child:  Text(
//                      'jkhjkh',
                      _name == null ? 'John Doe' : _name,
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0),
                    ),
                  ),
                  new Container(
                      margin: EdgeInsets.all(6.0),
                      child: new Flex(
                        mainAxisAlignment: MainAxisAlignment.center,
                        direction: Axis.horizontal,
                        children: <Widget>[
                          new Image.asset('resources/images/ic_active.png'),
                          new Text(
                            _status == null ? "Active" : _status == 'A' ? 'Active' : 'Inactive',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0),
                          ),
                        ],
                      ))
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(2.0, 24.0, 2.0, 0.0),
              child: new Flex(
                mainAxisAlignment: MainAxisAlignment.center,
                direction: Axis.horizontal,
                children: <Widget>[
                  new Text(
                    "Membership Number : ",
                    style: TextStyle(
                        color: CustomColors.appThemeBlue,
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0),
                  ),
                  new Text(
                    _cardNumber == null ? '4523542356' : _cardNumber,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                    ),
                  )
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(2.0, 24.0, 2.0, 0.0),
              child: new Flex(
                mainAxisAlignment: MainAxisAlignment.center,
                direction: Axis.horizontal,
                children: <Widget>[
                  new Text(
                    "Employee ID : ",
                    style: TextStyle(
                        color: CustomColors.appThemeBlue,
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0),
                  ),
                  new Text(
                    _staffId == null ? 'S5354345' : _staffId,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15.0,
                    ),
                  )
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
              child: new Flex(
                direction: Axis.horizontal,
                children: <Widget>[
                  new Image.asset(
                    'resources/images/dash.png',
                    width: screenWidth,
                  ),
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(2.0, 30.0, 2.0, 0.0),
              child: new Flex(
                mainAxisAlignment: MainAxisAlignment.center,
                direction: Axis.horizontal,
                children: <Widget>[
                  new Text(
                    "SCAN BARCODE",
                    style: TextStyle(
                        color: CustomColors.appThemeBlue,
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0),
                  ),
                ],
              ),
            ),
            new Container(
              child: _byteData == null
                  ? new Image.asset(
                'resources/images/img_barcode.png',
                width: 280.0,
                height: 130.0,
              )
                  : new Image.memory(_byteData, width: 280.0, height: 130.0),
            ),
            new Flex(
              direction: Axis.horizontal,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Container(
                    padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                    width: 50.0,
                    height: 50.0,
                    alignment: Alignment.center,
                    decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: CustomColors.gray,
                      borderRadius:
                      new BorderRadius.all(const Radius.circular(2.0)),
                      border: new Border.all(
                          color: CustomColors.borderGray,
                          width: 1.0,
                          style: BorderStyle.solid),
                    ),
                    child: new TextField(
                      textAlign: TextAlign.center,
                      controller: hourController,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                      ),
                      enabled: false,
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0),
                    )),
                new Container(
                  margin: EdgeInsets.all(10.0),
                  child: Text(
                    ":",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Colors.black),
                  ),
                ),
                new Container(
                    padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                    width: 50.0,
                    height: 50.0,
                    alignment: Alignment.center,
                    decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: CustomColors.gray,
                      borderRadius:
                      new BorderRadius.all(const Radius.circular(2.0)),
                      border: new Border.all(
                          color: CustomColors.borderGray,
                          width: 1.0,
                          style: BorderStyle.solid),
                    ),
                    child: new TextField(
                      textAlign: TextAlign.center,
                      controller: minutesController,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                      ),
                      enabled: false,
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0),
                    )),
                new Container(
                  margin: EdgeInsets.all(10.0),
                  child: Text(
                    ":",
                    style: new TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.0,
                        color: Colors.black),
                  ),
                ),
                new Container(
                    padding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 8.0),
                    width: 50.0,
                    height: 50.0,
                    alignment: Alignment.center,
                    decoration: new BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: CustomColors.gray,
                      borderRadius:
                      new BorderRadius.all(const Radius.circular(2.0)),
                      border: new Border.all(
                          color: CustomColors.borderGray,
                          width: 1.0,
                          style: BorderStyle.solid),
                    ),
                    child: new TextField(
                      textAlign: TextAlign.center,
                      controller: secondsController,
                      decoration: new InputDecoration(
                        border: InputBorder.none,
                      ),
                      enabled: false,
                      style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 20.0),
                    )),
              ],
            )
          ],
        ),
      ),
    );
  }
//  File _image;
//
//  Future getImage() async {
//    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
//
//    setState(() {
//      _image = image;
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return new Scaffold(
//      appBar: new AppBar(
//        title: new Text('Image Picker Example'),
//      ),
//      body: new Center(
//        child: _image == null
//            ? new Text('No image selected.')
//            : new Image.file(_image),
//      ),
//      floatingActionButton: new FloatingActionButton(
//        onPressed: getImage,
//        tooltip: 'Pick Image',
//        child: new Icon(Icons.add_a_photo),
//      ),
//    );
//  }
}

class UserData {
  int cardNumber = 0;
  String imageURL = '';
  String category = '';
  String status = '';
}
