import 'package:coldstorage_singapore/screens/LoginPage.dart';
import 'package:flutter/material.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';
import 'package:coldstorage_singapore/values/CustomColors.dart';
import 'package:coldstorage_singapore/screens/AboutUsPage.dart';
import 'package:coldstorage_singapore/screens/FaqPage.dart';
import 'package:coldstorage_singapore/screens/PrivacyPolicyPage.dart';
import 'package:coldstorage_singapore/screens/TermsAndConditionsPage.dart';
import 'package:coldstorage_singapore/screens/MyApp.dart';
import 'package:coldstorage_singapore/values/CustomIntegers.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() {
  runApp(new MaterialApp(
    home: new SettingsPage(),
  ));
}



class SettingsPage extends StatefulWidget {

  @override
  SettingsState createState() => SettingsState();
}


class SettingsState extends State<SettingsPage> {
  var _appBarTitle;
  static String care;

  @override
  void initState() {
    super.initState();
    _appBarTitle = CustomStrings.settings;

  }

  _navigateToAboutUs(){
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new AboutUsPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {

          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }
    ));
  }
  _navigateToPrivacyPolicy(){
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new PrivacyPolicyPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {

          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }
    ));
  }
  _navigateToFAQ(){
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new FaqPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {

          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }
    ));
  }
  _navigateToTerms(){
    Navigator.of(context).push(new PageRouteBuilder(
        opaque: true,
        transitionDuration: const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new TermsAndConditionsPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {

          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }
    ));
  }

  _saveLoggedInState() async{

    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setBool('isLoggedIn', false);
    _navigateToMyApp();
  }



  _navigateToMyApp(){

    Navigator.of(context).pushAndRemoveUntil(new PageRouteBuilder(
        opaque: true,
        transitionDuration:
        const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
        pageBuilder: (BuildContext context, _, __) {
          return new LoginPage();
        },
        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
          return new SlideTransition(
            child: child,
            position: new Tween<Offset>(
              begin: const Offset(2.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
          );
        }), (Route<dynamic> route) => false);
//    Navigator.of(context).push(new PageRouteBuilder(
//        opaque: true,
//        transitionDuration: const Duration(milliseconds: CustomIntegers.NAVIGATION_DELAY_TIME),
//        pageBuilder: (BuildContext context, _, __) {
//          return new MyApp();
//        },
//        transitionsBuilder: (_, Animation<double> animation, __, Widget child) {
//
//          return new SlideTransition(
//            child: child,
//            position: new Tween<Offset>(
//              begin: const Offset(2.0, 0.0),
//              end: Offset.zero,
//            ).animate(animation),
//          );
//        }
//    ));
  }




  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
//    return new Scaffold(body: new Text("gaasgag"),);

    return new Scaffold(
        appBar: new AppBar(
          title: new Text(_appBarTitle, style: TextStyle(fontSize: 15.0),),
          backgroundColor: CustomColors.appThemeBlue,
        ),
        body: new Column(
          children: <Widget>[
            new InkWell(

                onTap:(){ _navigateToAboutUs();},
                child: Container(
                  width: screenWidth,
                  height: 65.0,
                  color: Colors.white,
                  padding: EdgeInsets.all(20.0),
                  child: new Text(
                    ">  About us",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.black),
                  ),
                )),
            new Container(
              width: screenWidth,
              height: 1.0,
              color: CustomColors.gray,
            ),
            new InkWell(
                onTap:(){ _navigateToFAQ();},
                child: Container(
                  width: screenWidth,
                  height: 65.0,
                  color: Colors.white,
                  padding: EdgeInsets.all(20.0),
                  child: new Text(
                    ">  FAQ",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.black),
                  ),
                )),
            new Container(
              width: screenWidth,
              height: 1.0,
              color: CustomColors.gray,
            ),
            new InkWell(
                onTap:(){ _navigateToPrivacyPolicy();},
                child: Container(
                  width: screenWidth,
                  height: 65.0,
                  color: Colors.white,
                  padding: EdgeInsets.all(20.0),
                  child: new Text(
                    ">  Privacy policy",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.black),
                  ),
                )),
            new Container(
              width: screenWidth,
              height: 1.0,
              color: CustomColors.gray,
            ),
            new InkWell(
                onTap:(){ _navigateToTerms();},
                child: Container(
                  width: screenWidth,
                  height: 65.0,
                  color: Colors.white,
                  padding: EdgeInsets.all(20.0),
                  child: new Text(
                    ">  Terms and conditions",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.black),
                  ),
                )),
            new Container(
              width: screenWidth,
              height: 1.0,
              color: CustomColors.gray,
            ),
            new InkWell(
                onTap:(){ _saveLoggedInState();},
                child: Container(
                  width: screenWidth,
                  height: 65.0,
                  color: Colors.white,
                  padding: EdgeInsets.fromLTRB(40.0, 20.0,0.0,20.0),
                  child: new Text(
                    "Sign out",
                    textAlign: TextAlign.left,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16.0, color: Colors.red),
                  ),
                )),
            new Container(
              width: screenWidth,
              height: 1.0,
              color: CustomColors.gray,
            ),
          ],
        ));
  }

}
