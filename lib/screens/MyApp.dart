import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:coldstorage_singapore/networktask/Network.dart';
import 'package:coldstorage_singapore/values/CustomStrings.dart';

Future<Post> fetchPost() async {
  final response =
      await http.get('https://jsonplaceholder.typicode.com/posts/1');
  final responseJson = json.decode(response.body);

  return new Post.fromJson(responseJson);
}

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return new Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}

void main() => runApp(new MaterialApp(
      home: MyApp(),
    ));

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => new MyAppState();
}

class MyAppState extends State<MyApp> {
  Network network = new Network();
  bool _isChecked = false;

  void onChanged(bool value){
    setState(() {
      _isChecked = value;
    });
  }

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return new MaterialApp(
      title: 'Fetch Data Example',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Fetch Data Example'),
        ),
        body: new Center(
            child: new Column(
          children: <Widget>[
            new RaisedButton(
              onPressed: () => Navigator.pop(context),
              child: new Text("back"),
            ),
            new RaisedButton(
              onPressed: () => network.getJsonData(),
              child: new Text("JSON data"),
            ),
            new RaisedButton(
              onPressed: () => network.fraudUser("abc", "123", "ios"),
              child: new Text("fraud user"),
            ),
            new RaisedButton(
              onPressed: () => network.getStatus("S5454523D", "ST"),
              child: new Text("get status"),
            ),
            new RaisedButton(
              onPressed: () => network.postLoginData(
                  "S5454523D", "Test PPP", "+658860193702", "", "ST"),
              child: new Text("login"),
            ),
//            new RaisedButton(
//              onPressed: () => network.verifyOtp("S5454523D", "8196", "android",
//                  "kjdsfgjkgdsfkgjndjksfgnjkn", "ST"),
//              child: new Text("verify OTp"),
//            ),
            new Checkbox(value: _isChecked, onChanged: (bool value) {onChanged(value);}),

            new TextField(
              style: TextStyle(fontSize: 19.0,fontWeight: FontWeight.bold ),
//                decoration: const InputDecoration(
//                    labelText: 'Event name',
//                    filled: true
//                ),

            )
          ],
        )),
      ),
    );
  }

}
