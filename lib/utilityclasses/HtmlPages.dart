class HtmlPages {
  static const String faq = "<!doctype html>\n" +
      "<html>\n" +
      "<head>\n" +
      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
      "<title>FAQ</title>\n" +
      "<style>\n" +
      "body{\n" +
      "\tpadding:0;\n" +
      "\tmargin:0;\n" +
      "\tline-height: 1.4;\n" +
      "\t}\n" +
      "div{\n" +
      "\tpadding:15px;\n" +
      "\t}\t\n" +
      "\t\n" +
      "ul{\n" +
      "\t padding-left: 5px;\n" +
      "\t margin-top: 0;\n" +
      "   \n" +
      "\t\n" +
      "\t}\n" +
      "\th4{\n" +
      "\t\tmargin-bottom:5px;\n" +
      "\t\t}\n" +
      "\t\n" +
      ".applicationUseHeader{\n" +
      "\t    padding: 10px 0;\n" +
      "\t}\t\n" +
      "\tli\n" +
      "\t{\n" +
      "\t\tlist-style:none;\n" +
      "\t\t}\n" +
      "\t\n" +
      "\tul > li:before {\n" +
      "  content: \"-\";\n" +
      " padding-right: 5px;\n" +
      "}\n" +
      "\t@media screen and (max-width:440px){\n" +
      "\n" +
      "\th2{\n" +
      "    font-size: 18px;\n" +
      "}\n" +
      "h3{\n" +
      "\tfont-size:16px;}\n" +
      "\n" +
      "h4{\n" +
      "\t\n" +
      "\tfont-size:14px;}\n" +
      "\n" +
      "}\n" +
      "</style>\n" +
      "</head>\n" +
      "<body>\n" +
      "<div>\n" +
      "<h2>Registration and Login</h2>\n" +
      "\n" +
      "<h4>Where do I download the mobile app?</h4>\n" +
      "<ul>\n" +
      "<li>Search for xxx xxxx on iPhone App Store</li>\n" +
      "<li>Search for xxx xxxx on Android Play Store</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>What is the staff ID?</h4>\n" +
      "<ul>\n" +
      "<li>This is the staff ID that is issued by your company to you when you join the company. The format fullows your company's format.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Which name do I use for entry into the mobile app?</h4>\n" +
      "<ul>\n" +
      "<li>Use the name that your company registers you as an employee.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Why do you need to know my partial NRIC/FIN number?</h4>\n" +
      "<ul>\n" +
      "<li>This is to validate your access and ensure that your account is not unrightfully accessed by unauthorised personnel who just happens to guess your staff ID or name from which might be visible on your staff pass you carry around.\n" +
      "</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>If you already validate my Staff ID against name or NRIC/FIN, why do you then need to send OTP with my mobile number?</h4>\n" +
      "<ul>\n" +
      "<li>This is to ensure that we are able to identify users who have somehow managed to secure all previous pieces of information. Having a phone number to authenticate ensures that fraudulent users cannot abuse the system in a trivial manner.</li>\n" +
      "</ul>\n" +
      "\n" +
      "\n" +
      "<h2 class=\"applicationUseHeader\">Use of the application</h2>\n" +
      "<h4>What is the green border around my photo?</h4>\n" +
      "<ul>\n" +
      "<li>It indicates that the user has been authenticated and the discount card is valid.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>What is the red border around my photo?</h4>\n" +
      "<ul>\n" +
      "<li>A red border means that the user is not authorised or have been removed from the partner programme.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Why is my bar code greyed out?</h4>\n" +
      "<ul>\n" +
      "<li>Greyed out bar codes means the account is no longer valid and the cashier will not be able to scan the ID for discount.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Why is my photo blank or not updated?</h4>\n" +
      "<ul>\n" +
      "<li>It means that the photo was not updated backend. In a future enhancement, a feature will be added to allow you to change the photo that is used.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Why is there a ticking clock at the bottom of the barcode?</h4>\n" +
      "<ul>\n" +
      "<li>The ticking clock (down to the second), will enable our cashiers to identify at a glance that the card is valid and live.A non active clock will mean that the screen has been captured and could potentially invalidate the transaction or result in the account being suspended.</li>\n" +
      "</ul>\n" +
      "\n" +
      "<h4>Am I allowed to share my login details to friends and family?</h4>\n" +
      "<ul>\n" +
      "<li>Please check with your HR if you are permitted to do so. Note also that all mobile phones requiring the electronic card will have to perform login as well as be authenticated through 2FA SMS. You are reminded to safeguard your own personal details.</li>\n" +
      "</ul>\n" +
      "\n" +
      "</div>\n" +
      "</body>\n" +
      "</html>\n";




  static const String aboutUs = "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "<title>About Us</title>\n" +
      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
      "<style type=\"text/css\">\n" +
      "body{\n" +
      "\tpadding: 0 5% 0 5%;\n" +
      "\tline-height: 1.5;\n" +
      "\t\n" +
      "}\n" +
      "\n" +
      "</style>\n" +
      "</head>\n" +
      "<body>\n" +
      "<h4>Preferred Partners Programme (PPP) Mobile Application - About us</h4>\n" +
      "<p>With the Preferred Partners Programme, Dairy Farm is presenting the most value out of grocery and personal care shopping in Singapore. If you are enrolled on the programme through a preferred partner, you will be able to enjoy year-long savings when buying daily necessities such as your grocery and health and beauty needs at more than 240 locations across all Cold Storage, Jasons, Marketplace, Giant Express, Supermarkets & Hypermarkets and Guardian Health and Beauty stores. </p>\n" +
      "</body>\n" +
      "</html>";




  static const String privacyPolicy = "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\t<title>Privacy Policy</title>\n" +
      "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
      "\t\t<style type=\"text/css\">\n" +
      "body{\n" +
      "\tpadding: 20px;\n" +
      "    line-height: 1.5;\n" +
      "}\n" +
      "\n" +
      "h4{\n" +
      "\tpadding: 0;\n" +
      "\ttext-decoration: underline;\n" +
      "}\n" +
      "\n" +
      "span{\n" +
      "\ttext-decoration: underline;\n" +
      "\tfont-weight: bold;\n" +
      "}\n" +
      "ol{\n" +
      "\n" +
      "\tpadding: 0;\n" +
      "}\n" +
      "\n" +
      "ol ol li{\n" +
      "\tmargin-bottom: 10px;\n" +
      "}\n" +
      "\n" +
      "\n" +
      "ul{\n" +
      "\tlist-style-type: disc;\n" +
      "}\n" +
      ".numberList { counter-reset: item }\n" +
      ".numberList > li{ display: block }\n" +
      ".numberList > li:before { content: counters(item, \".\") \" \"; counter-increment: item }\n" +
      "@media screen and (max-width:680px)\n" +
      "{\n" +
      "\tbody{\n" +
      "padding: 5px;\n" +
      "}\n" +
      "\n" +
      "@media screen and (max-width:340px){\n" +
      "\n" +
      "\th2{\n" +
      "    font-size: 18px;\n" +
      "}\n" +
      "}\n" +
      "}\n" +
      "\n" +
      "</style>\n" +
      "</head>\n" +
      "<body>\n" +
      "<h2>Preferred Partners Programme (PPP) Mobile Application Privacy Policy</h2>\n" +
      "<h4>Online Privacy Policy</h4>\n" +
      "<p>We take your privacy seriously and we will only collect and use personal data as stated in this Privacy Policy.</p>\n" +
      "\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "\t<li><span>Collection and use of personal data</span>\n" +
      "\t<ol type=\"1\" class=\"numberList\">\n" +
      "\t<li>We will only use or disclose your personal data with your consent or as authorised by applicable laws and regulations.The personal data collected may include:\n" +
      "\t<ul >\n" +
      "\t\t<li>Name</li>\n" +
      "\t\t<li>Partial NRIC/ Passport Number</li>\n" +
      "\t\t<li>Dairy Farm/Jardines Matheson/Preferred Partner Employee number/ID</li>\n" +
      "\t\t<li>Mobile number</li>\n" +
      "\n" +
      "\t</ul>\n" +
      "\t</li>\n" +
      "\t<li>By creating a PPP account with us (whether directly or on your behalf), you consent to our collection, use and disclosure of your personal data for any or all of the purposes specified below:\n" +
      "<ul >\n" +
      "\t\t<li>Administer your online account with us</li>\n" +
      "\t\t<li>Provide the products or services purchased from us online (and for all other matters relating to such purchase)</li>\n" +
      "\t\t<li>Verify and carry out financial transactions in relation to payments made for online purchases from us</li>\n" +
      "\t\t<li>Managing, investigating or responding to feedback, request, claims or complaints provided or made by you</li>\n" +
      "\t\t<li>Audit the downloading of data from our application</li>\n" +
      "\t\t<li>Improve or customise the layout or content of our application</li>\n" +
      "\t\t<li>Identify visitors to our application</li>\n" +
      "\n" +
      "\t</ul>\n" +
      "\t</li>\n" +
      "\t<li>With your consent, we may collect, use and disclose your personal data for any or all of the purposes specified below:\n" +
      "\t<ul >\n" +
      "\t\t<li>Deliver marketing or promotional materials relating to our products, services or special offers (and other information which you have requested)</li>\n" +
      "\t\t<li>Inform you of updates to this application and accompany terms and conditions</li>\n" +
      "\t\t<li>Provide information or services relating to our products, promotions or offers</li>\n" +
      "\t\t<li>Conduct market surveys, research or data mining</li>\n" +
      "\n" +
      "\t</ul>\n" +
      "\n" +
      "\t</li>\n" +
      "\t<li>Your personal data will be retained by us and will be accessible to our employees and third parties engaged by us for any of the purposes stated in this Privacy Policy.You authorise us to (a) disclose all or any of your personal data to such third parties, and (b) collect your personal data from any other sources available to us (including credit referral agencies).</li>\n" +
      "\t<li>Insufficient or incorrect personal data provided to us by you may result in delays or failure in providing the products or services purchased by you.</li>\n" +
      "\t</ol>\n" +
      "\t</li>\n" +
      "\n" +
      "<li><span>Confidentiality of personal data</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "\t<li>Personal data provided to us by customers or users of this mobile application will be kept confidential and will only be disclosed for the purposes stated in this Privacy Policy. We may disclose personal data to third parties who perform certain tasks relating to the provision of our products or services to our customers or to users of this application. All such third parties are required by us to use the personal data strictly to provide the products or services, and are required to maintain the confidentiality of the personal data.</li>\n" +
      "\t<li> Any questions, comments, suggestions or information (other than personal data) sent or posted to this application by customers or users will be deemed voluntarily provided to us on a non-confidential and non-proprietary basis. We may use, reproduce, disclose, transmit, publish, broadcast or post elsewhere such information in connection with the development, manufacture and marketing of products or services, to meet the needs of our customers or visitors or for any other purpose.</li>\n" +
      "\t<li>We will only disclose your personal data without notice to you if required to do so by law, or in the good faith belief that such disclosure is necessary to:\n" +
      "<ul>\n" +
      "\t<li>Comply with legal process, or under compulsion of an order of court or governmental agency served on us or Cold Storage Singapore (1983) Pte Ltd</li>\n" +
      "\t<li>Protect our rights, property or safety or that of Cold Storage Singapore (1983) Pte Ltd, its employees, customers, visitors or others</li>\n" +
      "\n" +
      "</ul>\n" +
      "\t</li>\n" +
      "\t<li>This Privacy Policy applies only to personal data collected via this application.We are not responsible for the privacy practices or policies of other websites accessible via this application.By activating a link (eg, by clicking on the banner of an advertiser), you leave this application and we do not have control over any personal data or any other information you give to any other person or entity after you have left this application.</li>\n" +
      "\t</ol>\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Information collected by cookies</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li> Our application may pass a \"cookie\" (a string of information that is sent by a website access through the application to reside in your mobile phone's long term storage and/or temporarily in your mobile phone's memory). The purpose of a cookie is to tell the application server that you have returned to a particular page, to retain log-in information or to remember preferences and enhance your experience on our mobile application. We may allow third parties to set cookies through our application in order to help us manage and analyze the performance of our application. You will not be able to set the application to decline cookies.</li>\n" +
      "</ol>\t\n" +
      "\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Collection of computer data</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li>When you use this application, our company servers automatically records information that your mobile phone sends whenever you visit a mobile application.This information may include:\n" +
      "<ul >\n" +
      "\t\t<li>Your mobile phone's IP address</li>\n" +
      "\t\t<li>Operating System type</li>\n" +
      "\t\t<li>Location at which the application is being used</li>\n" +
      "\t\t<li>Pages visited within our application (including time spent on those pages)</li>\n" +
      "\t\t<li>Other items and information searched for on our application,access times and dates etc</li>\n" +
      "\n" +
      "\t</ul>\n" +
      "</li>\n" +
      "<li>This information is collected for analysis and evaluation in order to help us improve this application, and the services and products we provide.This information will not be used in association with any personal data.</li>\n" +
      "</ol>\t\n" +
      "\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Access request;Withdrawal of consent;Updating of personal data</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li>You may by contact us via email (csdpo@coldstorage.com.sg) or by telephone (1800 891 8100) to (a) request for access to your personal data,(b) withdraw your consent to our use or disclosure of your personal data, or (c) terminate your online account with us.</li>\n" +
      "<li>Requests to withdraw your consent to use or disclose your personal data may result in delays or failure to provide you with the products or services purchased from us.</li>\n" +
      "<li>You may also update or correct your personal data by accessing your online account with us.</li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Security of personal data</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li>Personal data collected by us will be (a) safely and securely stored, and (b) disposed when no longer needed by us for business or legal purposes.</li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Changes to Privacy Policy</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li> We may change this Privacy Policy at any time without prior notice to our customers or visitors. Any changes to this Privacy Policy will be published on this application without notification.</li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "\n" +
      "<li><span>Contact Us</span>\n" +
      "<ol type=\"1\" class=\"numberList\">\n" +
      "<li> If you have any questions about our data protection policy or practices, you may contact our Data Protection Officer via email (csdpo@coldstorage.com.sg) or by telephone (1800 891 8100). </li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "\n" +
      "\n" +
      "</ol>\n" +
      "</body>\n" +
      "</html>";




  static const String terms = "<!DOCTYPE html>\n" +
      "<html>\n" +
      "<head>\n" +
      "\t<title>Terms and Conditions</title>\n" +
      "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
      "\t<style type=\"text/css\">\n" +
      "body{\n" +
      "\tpadding: 20px;\n" +
      "    line-height: 1.5;\n" +
      "}\n" +
      "\n" +
      "h4{\n" +
      "\tpadding: 0 ;\n" +
      "}\n" +
      "ol{\n" +
      "\n" +
      "\tpadding: 0 0 0 16px ;\n" +
      "}\n" +
      "\n" +
      "\n" +
      "ol li{\n" +
      "\tmargin-bottom: 10px;\n" +
      "}\n" +
      "\n" +
      "ol li ol{\n" +
      "\n" +
      "\n" +
      "    padding: 0 0 0 15px;\n" +
      "}\n" +
      "\n" +
      ".numberList { counter-reset: item }\n" +
      ".numberList > li{ display: block }\n" +
      ".numberList > li:before { content: counters(item, \".\") \" \"; counter-increment: item }\n" +
      "\n" +
      "@media screen and (max-width:680px)\n" +
      "{\n" +
      "\tbody{\n" +
      "padding: 10px;\n" +
      "}\n" +
      "}\n" +
      "@media screen and (max-width:340px){\n" +
      "\n" +
      "\th2{\n" +
      "    font-size: 18px;\n" +
      "}\n" +
      "}\n" +
      "\n" +
      "</style>\n" +
      "</head>\n" +
      "<body>\n" +
      "<h2>Preferred Partners Programme (\"PPP\") Terms And Conditions</h2>\n" +
      "<h4>General</h4>\n" +
      "<ol type=\"1\">\n" +
      "  <li>In these Terms and Conditions the following expressions shall have the following meanings:\n" +
      "<ol type=\"a\">\n" +
      "  <li>\"eCard\" shall mean the electronic PPP discount card provisioned for by the mobile application for the Preferred Partners Programme.</li>\n" +
      "  <li>\"Customer\" shall mean you and all other persons who uses the electronic PPP card under your Login.</li>\n" +
      "  <li> \"Intellectual Property\" shall mean all intellectual property rights of whatsoever nature, including (without limitation) patents, designs, present and future trademark or copyright, whether or not registered or registrable by any means, and the right to file an application for registration thereof.</li>\n" +
      "  <li>\"Login\" shall mean a Customer's PPP number and access to the PPP application under that Customer's registration.</li>\n" +
      "  <li>\"Product/Products\" shall mean any and all the products for sale by Cold Storage.</li>\n" +
      "  <li>\"Cold Storage\" shall mean Cold Storage Singapore (1983) Pte Ltd who is the seller of the products sold under the Cold Storage, Jasons, Marketplace, Giant and/or Guardian brand names.</li>\n" +
      "  <li>\"Discount\" shall mean the percentage price off offered to the Customer on valid and qualifying purchases at Cold Storage.</li>\n" +
      "\n" +
      "</ol>\n" +
      "  </li>\n" +
      "  <li>All the offers presented by Cold Storage through this site are subject to these Terms and Conditions. By using the eCard provisioned by the mobile application by Cold Storage, you confirm and affirm that you have read, understood and agree to the Terms and Conditions in the form in which they appear at the time your details are submitted for application for the eCard.</li>\n" +
      "  <li>These terms and conditions shall prevail over any other document purporting to have a contractual effect including any Cold Storage store policy.</li>\n" +
      "  <li>The Customer shall agree to complete the registration process according to Cold Storage's requirements as stated on this site. The Customer shall provide Cold Storage with complete and updated information which shall include (but shall not be limited to) the Customer's legal name, Staff ID, partial NRIC number and mobile telephone where applicable.</li>\n" +
      "  <li>Registration to open only to employees and family members of Cold Storage, affiliate companies under the Jardines Matheson Holdings Limited and other preferred partners with a preferred partners status with Cold Storage. Cold Storage has the absolute discretion to refuse the registration of a potential customer for any reason whatsoever and to terminate the registration of any of its Customers for any reason whatsoever.</li>\n" +
      "  <li> The Customer will be liable for every order made under his Login and agrees to indemnify Cold Storage for all claims, damages whatsoever made by any third party arising from the actions of a person placing orders for the Products using the Customer's Login. The Customer shall be responsible for updating his registration information.</li>\n" +
      "  <li>Access to eCard in the PPP mobile Application is gained through use of a personal membership number. The Customer shall ensure that his membership number is kept strictly confidential to prevent unauthorized use. Cold Storage shall use its best endeavors to prevent any unauthorized entry to the PPP server application and database. However, Cold Storage shall not be liable for any damages or losses whatsoever if any third party gains unauthorized access to the PPP server application and database.</li>\n" +
      "\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>APPLICABLE DISCOUNTS AND PROMOTIONS</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>In the event of different special offers or promotions being made available to the Customer simultaneously for the same Product, the Customer may elect one such offer or promotion per Product but shall not be entitled to a combination of all such offers or promotions per Product.</li>\n" +
      "<li>Cold Storage may from time to time set credit limits for individual Customers and Cold Storage reserves the right to limit sales including the right to prohibit sales to re-sellers.</li>\n" +
      "<li>Cold Storage will institute a monthly limit on the amount of purchases that the PPP discount can be applied upon, on any or all categories of products. Cold Storage reserves the right to vary this amount and the categories that this covers, without any prior notification to the Customer.</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>PAYMENT</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>Payment for products made under the PPP programme cannot be made using the eCard, which is not a payment card. All existing payment methods available at existing checkouts are made available for PPP purchases.</li>\n" +
      "<li>Cold Storage may change at any time the method by which payments for Cold Storage orders may be effected and such changes may be notified to the Customer by e-mail, mail shots or by placing a notice to this effect on the Cold Storage stores or through any other electronic means.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>WARRANTIES</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>To the extent permitted by law, all warranties, descriptions, representations or advice given as to the fitness or suitability for any purpose, tolerance to any conditions, similarity to sample, merchantability or otherwise of the Products supplied, are expressly excluded.</li>\n" +
      "<li>No agent or representative of Cold Storage is authorized to make any warranties, representations or statements regarding the Products and Cold Storage shall not in any way be bound by any such unauthorized warranties, representations or statements.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>SPECIAL PRODUCTS</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>If the Customer's purchase includes any tobacco product then the Customer warrants that he/she is 18 years of age or over and legally entitled to purchase any tobacco product.</li>\n" +
      "<li>If the Customer's purchase includes any liquor product the Customer warrants that he/she is not under the age of 18 years and is legally entitled to purchase any liquor product and that he/she is not under the influence of alcohol at the time of purchasing.</li>\n" +
      "<li>The Customer shall ensure that no person or persons shall use his/her PPP login application to purchase tobacco products or alcohol if that person or persons cannot satisfy Clauses 1 and 2 under SPECIAL PRODUCTS.</li>\n" +
      "<li>Use of the PPP mobile application and a valid barcode displayed with the application does not imply compliance with any legislations and entitlement to purchase and or consume and SPECIAL PRODUCTS.</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>EXCLUSIONS</h4>\n" +
      "<ol type=\"1\" class=\"numberList\" style=\"padding: 0px;\">\n" +
      "<li>The following products are not entitled to any form of Discounts implied through the use of the PPP application:\n" +
      "<ol class=\"numberList\" sty>\n" +
      "<li>- tobacco products;</li>\n" +
      "<li>- infant milk powder;</li>\n" +
      "<li>- statutory products;</li>\n" +
      "<li>- concessionary booth sales;</li>\n" +
      "<li>- already discounted products from DFSG's corporate brands range paid for using the UOB Delight Card; and</li>\n" +
      "<li>- products purchased from the on-line stores of \"Cold Storage\", \"Giant\" and \"Guardian\".</li>\n" +
      "\n" +
      "</ol>\n" +
      "</li>\n" +
      "<li>The Preferred Partner Programme and the Discount shall be at Cold Storage, Market Place, Jason supermarkets, Giant Hyper, Super and Express stores, Guardian Health & Beauty and not apply to products purchased at 7-Eleven stores.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>INTELLECTUAL PROPERTY</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>All right, title and interest in all Intellectual Property in all concepts, systems, written, graphic and other material relating to PPP shall at all times remain the property of Cold Storage.</li>\n" +
      "<li>Cold Storage and other parties own the trade marks, logos and service marks displayed on this site/catalogue and/or in connection with the Products and Customers and mobile application users are prohibited from using the same without the written permission of Cold Storage or such other parties.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>FORCE MAJEURE</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>If the performance by Cold Storage of its obligations under the PPP is prevented by reason of \"force majeure\" (which shall include prevention occasioned by fire, casualty, accident, act of God, natural disaster, any law, order, proclamation, regulation, demand or requirement of the government of Singapore or of any of its government agencies, strikes, labour disputes, shortage of labour or lack of skilled labour, shortage or unavailability of products or raw materials, delay in transit or other causes whatsoever (whether similar to the foregoing or not) beyond the reasonable control of Cold Storage), Cold Storage shall be excused from such performance to the extent of such prevention.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>VARIATION OF TERMS AND CONDITIONS</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>Cold Storage reserves the right to amend these Terms and Conditions in its sole discretion. Cold Storage may, but does not undertake, to notify the Customer of such changes. It is the Customer's responsibility to review the Terms and Conditions prior to submitting any order and Cold Storage shall have no responsibility to notify the Customer of any changes to the Terms and Conditions prior to the coming into effect of such changes or otherwise.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>APPLICABLE LAW</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>These Terms and Conditions shall be governed by the laws of Singapore and the Customer and Cold Storage agree to submit to the jurisdiction of the Singapore courts.</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>CONFIDENTIALITY</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>Cold Storage does not sell, rent or lease its Customer lists to third party.</li>\n" +
      "<li>Personal data held by Cold Storage relating to its Customers or to visitors to this site will be kept confidential unless Cold Storage has Customer's consent otherwise but Cold Storage may provide this information to third parties including but not limited to Cold Storage advertisers, associated companies or trusted contractors/agents or other network operators who perform certain tasks for Cold Storage in connection with the provision of Cold Storage's services to its Customers and to visitors to this site. All such third parties are prohibited from using Customer's personal data except to provide the requisite services to Cold Storage and they are required to maintain the confidentiality of such data.</li>\n" +
      "<li>Any questions, comments, suggestions or information other than personal data sent or posted to this site, or any part of this site by online Customers and visitors will be deemed voluntarily provided to Cold Storage on a non-confidential and non-proprietary basis. Cold Storage reserves the right to use, reproduce, disclose, transmit, publish, broadcast and/or post elsewhere such information freely, including passing it to any associated company for example, in connection with the development, manufactured and marketing of products and services and to meet Customer needs.</li>\n" +
      "<li> Cold Storage will disclose Customer's personal data, without notice, only if required to do so by law or in the good faith belief that such action is necessary to:-\n" +
      "<ol type=\"a\">\n" +
      "<li>TComply with legal process served on Cold Storage or the site;</li>\n" +
      "<li>Protect and defend the rights or property of Cold Storage; and</li>\n" +
      "<li>Act under exigent circumstances to protect the personal safety of users of Cold Storage, PPP Customer or the public.</li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>SAFETY OF PERSONAL DATA</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>All reasonable efforts are made by Cold Storage to ensure that any personal data held by Cold Storage and Cold Storage Online is stored in a secure and safe place and no unauthorised access will be made by Cold Storage to Customer's computer or browser.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>LINKS</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>The PPP mobile application may contain links to other sites and pages. By activating a link, such as for example by clicking on the banner of an advertiser, the Customer leaves Cold Storage’s site and Cold Storage does not exercise control over any personal data or any other information the Customer gives to any other entity after leaving the site and access to such site or page is at the Customer’s own risk for which Cold Storage disclaims all responsibility and liability.</li>\n" +
      "</ol>\n" +
      "\n" +
      "<h4>CONTENT</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>All reasonable efforts are made to ensure that the information provided on the PPP mobile application is correct and up-to-date. However, Cold Storage disclaims all implied and/or express warranties and makes no representation as to the accuracy or completeness of any information on the site. Cold Storage takes no responsibility and assumes no liability for the content of the PPP mobile application or for anything posted on or linked to it, including without limitation any mistake, error, omission, infringement, defamation, falsehood, or other material or omission that might offend or otherwise give rise to any claim or complaint. (See also without limitation the words in capitals below)</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>SECURITY POLICY</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>Cold Storage had established an information security policy for the prevention and detection of unauthorized access or misuse of information with the following methods:\n" +
      "<ol type=\"a\">\n" +
      "<li>Firewalls facility.</li>\n" +
      "<li>Customers information and private transaction data are protected using 128-bit Secure Socket Layer (SSL) encryption. SSL encryption is an Internet security protocol used by Internet browsers to transmit sensitive information.</li>\n" +
      "<li>We test our systems periodically to ensure there are no possible breaches of our security and that our customers' security and confidential policy information is not compromised in any way.</li>\n" +
      "<li>Should you encounter any virus outbreak, security breaches or received unsolicited mail, please call our Customer Service Hotline: (65)6891 8100 or write to the following address:\n" +
      "<p>Attn: IT Security Officer</p>\n" +
      "<p>21 Tampines North Drive</p>\n" +
      "<p>2 #03-01</p>\n" +
      "<p>Singapore 528765</p>\n" +
      "</li>\n" +
      "</ol>\n" +
      "</li>\n" +
      "</ol>\n" +
      "\n" +
      "\n" +
      "<h4>LIMITATION OF LIABILITY</h4>\n" +
      "<ol type=\"1\">\n" +
      "<li>By accessing and using this Cold Storage Online internet site, the Customer acknowledges and accepts that the use of the site is at the Customer’s own risk. Cold Storage shall not be liable for any direct, indirect, incidental, consequential or punitive damage or for damages for lost profits or for loss of revenue arising out of any use of, access to, or inability to use the site. Without limiting the foregoing:</li>\n" +
      "</ol>\n" +
      "<p>HIS SITE AND ALL INFORMATION AND MATERIALS CONTAINED IN IT ARE PROVIDED \"AS IS\" WITHOUT ANY WARRANTY OF ANY KIND EITHER EXPRESS OR IMPLIED INCLUDING BUT NOT LIMITED TO ANY IMPLIED WARRANTIES OR IMPLIED TERMS AS TO TITLE, QUALITY, MERCHANTABILITY, FITNESS FOR PURPOSE , PRIVACY OR NON-INFRINGEMENT. COLD STORAGE HAS NO LIABILITY OR RESPONSIBILITY FOR ANY ERRORS OR OMISSIONS IN THE CONTENTS OF THE SITE. COLD STORAGE ASSUMES NO RESPONSIBILITY AND SHALL NOT BE LIABLE (TO THE EXTENT PERMITTED BY LAW) FOR ANY DAMAGE OR INJURY ARISING OUT OF ANY USE OF OR ACCESS TO THE SITE/CATALOGUE, OR ANY FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMPUTER VIRUS, COMMUNICATION LINE FAILURE, INTERCEPTION OF ONLINE COMMUNICATION, SOFTWARE OR HARDWARE PROBLEMS (INCLUDING WITHOUT LIMITATION LOSS OF DATA OR COMPATIBILITY PROBLEMS), THEFT, DESTRUCTION OR ALTERATION OF THE SITE, WHETHER FOR BREACH OF CONTRACT, TORTIOUS BEHAVIOUR, NEGLIGENCE OR, UNDER ANY OTHER CAUSE OF ACTION RESULTING DIRECTLY OR INDIRECTLY FROM ANY ACCESS OR USE OF THE SITE/CATALOGUE, OR ANY UPLOADING, DOWNLOADING OR PUBLICATION OF DATA, TEXT, IMAGES OR OTHER MATERIAL OR INFORMATION TO OR FROM THE SITE.</p>\n" +
      "\n" +
      "<p>Copyright ©, 1997-2017.</p>\n" +
      "<p>Cold Storage All rights reserved</p>\n" +
      "</body>\n" +
      "</html>";
}