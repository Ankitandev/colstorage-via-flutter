import 'package:flutter/material.dart';

class CustomColors {
  static const Color gray = const Color(0xFFF1F5F7);
  static const Color borderGray = const Color(0xFFe2e2e2);
  static const Color roundBlue = const Color(0xFF104777);
  static const Color buttonRed = const Color(0xFFe41e26);
  static const Color appThemeBlue = const Color(0xFF104777);
}