///
/// Custom strings class for strings
///
class CustomStrings{


  static const String URL_JSON_DATA = "https://jsonplaceholder.typicode.com/posts/1";
  static const String BASE_URL = "http://54.254.255.202:8080";

  static const String termsAndConditions = "TERMS AND CONDITIONS";
  static const String privacyPolicy = "PRIVACY POLICY";
  static const String faq = "FAQ";
  static const String aboutUs = "ABOUT US";
  static const String settings = "SETTINGS";
  static const String tncClose = "CLOSE";

  static const String CS_CHANNEL = "sample.flutter.io/cs";


  static const String PREF_ABOUT_US = "aboutUs";
  static const String PREF_TERMS_AND_CONDITIONS = "terms";
  static const String PREF_PRIVACY_POLICY = "policy";
  static const String PREF_FAQ = "faq";
}