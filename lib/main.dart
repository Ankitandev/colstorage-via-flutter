import 'package:coldstorage_singapore/screens/ProfilePage.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:coldstorage_singapore/screens/LoginPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  print('hello');
//  runApp(new MaterialApp(
//    home: new LoginPage(),
////    routes: <String, WidgetBuilder> { //5
////      '/LoginPage': (BuildContext context) => new LoginPage(), //6
////    },
//
//  ));
  isUserLoggedIn();
}

isUserLoggedIn() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  var flag = preferences.getBool('isLoggedIn');
  print(flag);
  flag == null?runApp(new MaterialApp(
    home: new LoginPage(),
  )):
  runApp(new MaterialApp(
    home: flag ? new ProfilePage() : new LoginPage(),
  ));
}

//class SplashScreen extends StatefulWidget {
//
//
//  @override
//  SplashState createState() => new SplashState();
//
//}
//
//class SplashState extends State<SplashScreen> {
//
//  @override
//  void initState() {
//    super.initState();
//    startTime();
//  }
//
//  startTime() async {
//    var _duration = new Duration(seconds: 3);
//    return new Timer(_duration, navigationPage);
//  }
//
//
//  void navigationPage() {
//    Navigator.popAndPushNamed(context, "/LoginPage");
//  }
//
//
//
//
//
//
//  @override
//  Widget build(BuildContext context) {
//    double screenWidth = MediaQuery.of(context).size.width;
//    double screenHeight = MediaQuery.of(context).size.height;
//    return new Scaffold(
//      body: new Container(
//        width: screenWidth,
//        height: screenHeight,
//        child: new Image.asset('resources/images/splash.png', fit: BoxFit.fitHeight,),
//      ),
//    );
//  }
//
//}
