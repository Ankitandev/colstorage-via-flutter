package com.affle.coldstoragesingapore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.Toast;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

import com.affle.coldstoragesingapore.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;


public class MainActivity extends FlutterActivity {

    private static final String CS_CHANNEL = "sample.flutter.io/cs";
    private String toastText;
    private String cardNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);

        new MethodChannel(getFlutterView(), CS_CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        if (call.method.equals("createToast")) {
                            toastText = call.argument("text");
                            createToast();
                            result.success("success");
                        }
                        else if (call.method.equals("getBarcode")) {
                            cardNumber = call.argument("id");
                            byte[] bytes = getBarcode(cardNumber);
                            result.success(bytes);
                        }
                        else {
                            result.notImplemented();
                        }
                    }
                }
        );

//        new MethodChannel(getFlutterView(), CS_CHANNEL).setMethodCallHandler(
//                new MethodChannel.MethodCallHandler() {
//                    @Override
//                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
//                         else {
//                            result.notImplemented();
//                        }
//                    }
//                }
//        );
    }


    private void createToast() {
        Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_SHORT).show();

    }

    private byte[] getBarcode(String membershipId){

        Bitmap bmp = generateBarcode(String.valueOf(membershipId));
        if (bmp != null){
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            bmp.recycle();
            return byteArray;
        }
        return null;
    }

    /***
     *  method to generate barcode on Image view
     * @param mMembershipId generate barcode for its respective membership ID
     */
    private Bitmap generateBarcode(String mMembershipId) {

        Bitmap bitmap;
        int widthInPixels = getResources().getDimensionPixelSize(R.dimen.width_generated_barcode);
        int heightInPixels = getResources().getDimensionPixelSize(R.dimen.height_generated_barcode);
        try {
            if (mMembershipId.length() == 12) {
                calculateChecksumDigit(mMembershipId);
                bitmap = BarcodeGenerator.encodeAsBitmap(mMembershipId + calculateChecksumDigit(mMembershipId), BarcodeFormat.EAN_13, widthInPixels, heightInPixels);
                if (bitmap != null) return bitmap;
                else return  null;
            } else if (mMembershipId.length() < 12) {
                /*
                 * adding leading 0's if the digits in id are less than 12
                 */
                for (int i = 0; i < 12 - mMembershipId.length(); i++) {
                    mMembershipId = "0" + mMembershipId;
                    calculateChecksumDigit(mMembershipId);
                    bitmap = BarcodeGenerator.encodeAsBitmap(mMembershipId + calculateChecksumDigit(mMembershipId), BarcodeFormat.EAN_13, widthInPixels, heightInPixels);
                    if (bitmap != null) return bitmap;
                    else return  null;
                }
            } else {
                bitmap = BarcodeGenerator.encodeAsBitmap(mMembershipId, BarcodeFormat.CODE_128, widthInPixels, heightInPixels);
                if (bitmap != null) return bitmap;
                else return  null;
            }
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Returns checksum digit calculated using 12 digit of input string for EAN-13 barcode format
     * @param mMembershipId employee id
     * @return check sum
     */
    private int calculateChecksumDigit(String mMembershipId) {
        int total_sum = 0;
        for (int i = 0; i < mMembershipId.length(); i++) {
            if (i % 2 != 0) {
                total_sum = total_sum + Integer.parseInt("" + mMembershipId.charAt(i)) * 3;
            } else {
                total_sum = total_sum + Integer.parseInt("" + mMembershipId.charAt(i));
            }
        }
        if (total_sum % 10 == 0) {
            return total_sum % 10;
        } else {
            return 10 - (total_sum % 10);
        }

    }


}
